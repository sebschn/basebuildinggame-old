﻿using UnityEngine;

[CreateAssetMenu(fileName = "GroundTileBlueprint", menuName = "Blueprints/GroundTile", order = 0)]
public class GroundTileBlueprint : ScriptableObject
{
    [Header( "Base" )]
    public Tile.TileType type;
    public Sprite sprite;

    [Header( "Variables" )]
    public float movementPenalty;
}
