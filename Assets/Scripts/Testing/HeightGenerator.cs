﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeightGenerator : MonoBehaviour
{
    [System.Serializable]
    public class Modifier
    {
        public Vector2 range = new Vector2( 0, 0 );
        [Range( 0f, 1f )]
        public float scale = 0.1f;
        [Range( 0f, 10f )]
        public float strength = 1f;
    }

    public class Cube
    {
        public GameObject obj;
        private float height;
        public Action<Cube> onHeightChanged;

        public Cube( GameObject obj, float height )
        {
            this.obj = obj;
            this.height = height;
        }

        public void SetHeight( float value, float threshold )
        {
            height = value;

            if ( height > threshold )
                obj.SetActive( true );
            else
                obj.SetActive( false );
        }
    }

    [Header( "Noise Modifiers" )]
    public List<Modifier> modifiers;

    [Header( "Height Threshold" )]
    [Range(0, 10)]
    public float heightThreshold = 0.5f;

    [Header( "Object" )]
    public GameObject cube;
    private Cube[,] cubes;

    private int width;
    private int height;

    private void Start()
    {
        width = 50;
        height = 50;

        cubes = new Cube[width, height];

        for ( int x = 0; x < height; x++ )
        {
            for ( int y = 0; y < width; y++ )
            {
                cubes[x, y] = new Cube( Instantiate( cube, new Vector3( x, y, 0 ), Quaternion.identity ), 0 );
                cubes[x, y].obj.name = $"Cube {x}, {y}";
                cubes[x, y].obj.transform.SetParent( transform );
            }
        }
    }

    private void Update()
    {
        SetHeightPerlin();
    }

    private void SetHeightPerlin()
    {
        float perlin = 0;

        for ( int x = 0; x < this.width; x++ )
        {
            for ( int y = 0; y < this.height; y++ )
            {
                foreach ( Modifier modifier in modifiers )
                {
                    perlin += Mathf.PerlinNoise( (x + modifier.range.x) * modifier.scale, (y + modifier.range.y) * modifier.scale ) * modifier.strength;
                }

                cubes[x, y].SetHeight( perlin, heightThreshold );
                perlin = 0;
            }
        }
    }
}
