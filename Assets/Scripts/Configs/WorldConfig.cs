﻿using UnityEngine;

[CreateAssetMenu(fileName = "WorldConfig", menuName = "Configs/World/WorldConfig", order = 0)]
public class WorldConfig : ScriptableObject 
{
    public int width;
    public int height;
}
