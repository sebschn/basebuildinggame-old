using UnityEngine;
using System.Collections.Generic;

public static class SimplePool 
{

    /*
     * To Instantiate an Object call
     * SimplePool.Instantiate( GameObject, Vector3, Quaternion );
     *
     * To Destroy an Object call
     * SimplePool.Destroy( GameObject );
    */

    class Pool 
    {
        Stack<GameObject> inactives;

        GameObject prefab;

        int nextId = 1;

        public Pool ( GameObject prefab )
        {
            this.prefab = prefab;
            inactives = new Stack<GameObject>();
        }

        public GameObject Spawn ( Vector3 position, Quaternion rotation )
        {
            GameObject obj;

            if ( inactives.Count == 0 ) 
            {
                obj = (GameObject) GameObject.Instantiate( prefab, position, rotation );
                obj.name = prefab.name + $" ({nextId++}) ";

                obj.AddComponent<PoolMember>().myPool = this;
            } 
            else 
            {
                obj = inactives.Pop();

                if ( obj == null )
                    return Spawn( position, rotation );
            }

            obj.transform.position = position;
            obj.transform.rotation = rotation;
            obj.SetActive( true );
            return obj;
        }

        public void Despawn ( GameObject obj )
        {
            obj.SetActive( false );
            inactives.Push( obj );
        }
    }

    class PoolMember : MonoBehaviour 
    {
        public Pool myPool;
    }

    private static Dictionary<GameObject, Pool> pools;

    private static void Init ( GameObject prefab = null )
    {
        if ( pools == null )
            pools = new Dictionary<GameObject, Pool>();

        if ( prefab != null && pools.ContainsKey( prefab ) == false )
            pools[prefab] = new Pool( prefab );
    }

    public static GameObject Instantiate ( GameObject prefab, Vector3 position, Quaternion rotation )
    {
        Init( prefab );

        return pools[prefab].Spawn( position, rotation );
    }

    public static void Destroy ( GameObject obj )
    {
        PoolMember pm = obj.GetComponent<PoolMember>();

        if ( pm == null )
            GameObject.Destroy( obj );
        else 
            pm.myPool.Despawn( obj );
    }
}