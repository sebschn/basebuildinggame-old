﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Area2D : IEquatable<Area2D>
{
    public Vector2 start { get; set; }
    public Vector2 end { get; set; }

    public delegate void LoopMethod( int x, int y );
    public delegate void LoopMethodVector( Vector2 position );

    public Area2D( Vector2 start, Vector2 end )
    {
        this.start = start;
        this.end = end;
    }

    /// <summary>
    /// Loops through the area and executes the given LoopMethod for every Tile
    /// </summary>
    /// <param name="loopMethod">Method to invoke each iteration</param>
    public void Loop( LoopMethod loopMethod ) => LoopThroughArea( loopMethod );

    /// <summary>
    /// Loops through the area and executes the given LoopMethod for every Tile
    /// </summary>
    /// <param name="loopMethod">Method to invoke each iteration, takes in a Vector</param>
    public void Loop( LoopMethodVector loopMethod ) => LoopThroughArea( ( x, y ) => loopMethod( new Vector2( x, y ) ) );

    private void LoopThroughArea( LoopMethod loopMethod )
    {
        int startX = Mathf.RoundToInt( this.start.x );
        int endX = Mathf.RoundToInt( this.end.x );

        if ( endX < startX )
        {
            int tmp = endX;
            endX = startX;
            startX = tmp;
        }

        int startY = Mathf.RoundToInt( this.start.y );
        int endY = Mathf.RoundToInt( this.end.y );

        if ( endY < startY )
        {
            int tmp = endY;
            endY = startY;
            startY = tmp;
        }

        for ( int x = startX; x <= endX; x++ )
        {
            for ( int y = startY; y <= endY; y++ )
            {
                loopMethod.Invoke( x, y );
            }
        }
    }

    public bool Equals( Area2D other )
    {
        if ( ReferenceEquals( null, other ) )
            return false;

        if ( ReferenceEquals( this, other ) )
            return true;

        return ( start.Equals( other.start ) && end.Equals( other.end ) );
    }

    public override bool Equals( object area )
    {
        if ( ReferenceEquals( null, area ) )
        {
            return false;
        }
        if ( ReferenceEquals( this, area ) )
        {
            return true;
        }

        return area.GetType() == GetType() && Equals( (Area2D) area );
    }

    public static bool operator ==( Area2D area1, Area2D area2 )
    {
        if ( ReferenceEquals( area1, area2 ) )
            return true;

        if ( ReferenceEquals( area1, null ) )
            return false;

        if ( ReferenceEquals( area2, null ) )
            return false;

        return ( area1.start == area2.start && area1.end == area2.end );
    }

    public static bool operator !=( Area2D area1, Area2D area2 )
    {
        return !( area1 == area2 );
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
