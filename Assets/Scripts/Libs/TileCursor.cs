﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileCursor
{
    /*
     * Tile Cursor 
    */

    private GameObject cursorObject;
    private SpriteRenderer spriteRenderer;

    private Sprite sprite;

    public TileCursor ( Sprite sprite, Transform parent )
    {
        CreateTileCursor( parent );
        ChangeCursorSprite( sprite );
    }

    public TileCursor( Vector3 position, Sprite sprite, Transform parent ) : this( sprite, parent )
    {
        UpdateCursorPosition( position );
    }

    public void Enable() => cursorObject.SetActive( true );
    public void Disable() => cursorObject.SetActive( false );

    public void UpdateCursorPosition( Vector3 position )
    {
        cursorObject.transform.position = position;
    }

    public void ChangeCursorSprite( Sprite sprite )
    {
        this.sprite = sprite;
        spriteRenderer.sprite = sprite;
    }

    private void CreateTileCursor( Transform parent )
    {
        cursorObject = new GameObject();
        cursorObject.name = "Tile Cursor";
        cursorObject.transform.SetParent( parent );

        spriteRenderer = cursorObject.AddComponent<SpriteRenderer>() as SpriteRenderer;
        spriteRenderer.sortingLayerName = SortingLayers.TileUI.ToString();
    }
}
