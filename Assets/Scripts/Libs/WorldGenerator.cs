﻿using System;
using UnityEngine;

public static class WorldGenerator
{
    static World world;
    static Tile[,] tiles;

    public static World GetWorld ( WorldConfig blueprint ) 
    {
        world = new World( blueprint.width, blueprint.height );

        GenerateWorld();

        return world;
    }

    private static void GenerateWorld()
    {
        tiles = new Tile[world.width, world.height];

        for ( int x = 0; x < world.width; x++ )
        {
            for ( int y = 0; y < world.height; y++ )
            {
                CreateTile( x, y );
            }
        }

        world.SetTiles( tiles );
    }

    private static void CreateTile ( int x, int y ) 
    {
        float height = GetTileHeightPerlinNoise( x, y );
        tiles[x, y] = new Tile( world, x, y, height );
        tiles[x, y].Type = GetRandomTileType();
    }

    private static float GetTileHeightPerlinNoise( int x, int y )
    {
        float perlin = Mathf.PerlinNoise( x * 0.1f, y * 0.1f );
        perlin += Mathf.PerlinNoise( x * 0.2f, y * 0.2f );

        return perlin;
    }

    private static Tile.TileType GetRandomTileType () 
    {
        Array values = Enum.GetValues( typeof(Tile.TileType) );
        return (Tile.TileType)values.GetValue( UnityEngine.Random.Range( 0, values.Length ) );
    } 
}
