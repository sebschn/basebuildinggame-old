﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BuildingPreview
{
    private class PreviewCursor
    {
        private GameObject cursorObject;
        private SpriteRenderer spriteRenderer;

        public PreviewCursor( Vector2 position, Sprite sprite, GameObject parent = null )
        {
            CreateGameObject( parent );
            SetUp( position, sprite );
        }

        public void Disable() => cursorObject.SetActive( false );
        public void Enable() => cursorObject.SetActive( true );

        private void CreateGameObject( GameObject parent = null )
        {
            cursorObject = new GameObject();
            cursorObject.name = "Building Preview Cursor";

            if ( parent != null )
                cursorObject.transform.SetParent( parent.transform );

            spriteRenderer = cursorObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = SortingLayers.TileUI.ToString();
        }

        public void SetUp( Vector2 position, Sprite sprite )
        {
            if ( cursorObject == null )
                CreateGameObject();

            spriteRenderer.sprite = sprite;
            cursorObject.transform.position = position;
        }
    }

    private static GameObject cursorParent;
    private static Sprite previewSprite;

    private static List<PreviewCursor> availableCursors = new List<PreviewCursor>();
    private static List<PreviewCursor> previewCursors = new List<PreviewCursor>();

    private static Area2D currentPreviewArea;

    public static void SetCursorParent( GameObject cursorParent )
    {
        BuildingPreview.cursorParent = cursorParent;
    }

    public static void CreateArea( Area2D newPreviewArea, Sprite sprite )
    {
        // TODO: Change how BuildingPreview Sprites are handled
        previewSprite = sprite; 

        if ( currentPreviewArea != null )
            if ( currentPreviewArea == newPreviewArea )
                return;

        currentPreviewArea = newPreviewArea;
        ClearCurrentCursors();

        newPreviewArea.Loop( ShowCursor );
    }

    public static void ClearArea()
    {
        ClearCurrentCursors();
    }

    private static void ClearCurrentCursors()
    {
        if ( previewCursors.Count <= 0 )
            return;

        foreach ( PreviewCursor cursor in previewCursors )
        {
            cursor.Disable();
            availableCursors.Add( cursor );
        }

        previewCursors.Clear();
    }

    private static void ShowCursor( Vector2 position )
    {
        if ( WorldController.Instance.GetTileAtWorldPosition( position ) == null )
            return;

        PreviewCursor cursor;

        if ( availableCursors.Count > 0 )
        {
            cursor = availableCursors[0];
            availableCursors.RemoveAt( 0 );
            cursor.Enable();
        }
        else
        {
            cursor = new PreviewCursor( position, previewSprite, cursorParent );
        }

        cursor.SetUp( position, previewSprite );
        previewCursors.Add( cursor );
    }
}
