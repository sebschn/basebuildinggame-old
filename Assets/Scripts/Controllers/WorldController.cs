﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour
{
    public World World { get; private set; }

    [Header( "World" )]
    [SerializeField] WorldConfig worldConfig;

    [Header( "Sprites" )]
    [SerializeField] List<GroundTileBlueprint> groundTileBlueprints;

    public static WorldController Instance { get; private set; }

    void Start ()
    {
        if ( Instance != null )
            Debug.LogError( "An instance of WorldController already exists!" );

        Instance = this;

        if ( World == null )
            World = WorldGenerator.GetWorld( worldConfig );   

        CreateWorld(); 
    }

    private void CreateWorld () 
    {
        for (int x = 0; x < World.width; x++)
        {
            for (int y = 0; y < World.height; y++)
            {
                Tile tileData = World.GetTileAt( x, y );

                GameObject tileGO = new GameObject();
                tileGO.name = x + "_" + y;
                tileGO.transform.parent = this.transform;
                tileGO.transform.position = new Vector3( tileData.x, tileData.y, 0 );

                tileGO.AddComponent<SpriteRenderer>();

                tileData.onTileTypeChanged += ( sender, tile ) => { Tile_OnTileTypeChanged( sender, tile, tileGO ); };

                SetGroundTileSprite( tileGO, tileData.Type );
            }
        }
    }

    public Tile GetTileAtWorldPosition( Vector2 position ) => RoundToAndGetTileAtPosition( position.x, position.y );
    public Tile GetTileAtWorldPosition( Vector3 position ) => RoundToAndGetTileAtPosition( position.x, position.y );

    private Tile RoundToAndGetTileAtPosition( float x, float y )
    {
        int roundedX = Mathf.RoundToInt( x );
        int roundedY = Mathf.RoundToInt( y );

        return World.GetTileAt( roundedX, roundedY );
    }

    private void Tile_OnTileTypeChanged ( object sender, Tile tileData, GameObject tileGO ) 
    {
        SetGroundTileSprite( tileGO, tileData.Type );
    }

    private void SetGroundTileSprite ( GameObject tileGO, Tile.TileType type ) 
    {
        Sprite sprite = groundTileBlueprints.Find( x => x.type == type ).sprite;
        tileGO.GetComponent<SpriteRenderer>().sprite = sprite;
    }
}
