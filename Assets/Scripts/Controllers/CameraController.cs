﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [Header( "Camera Scroll" )]
    [SerializeField] private float keyboardScrollSpeed = 20f;

    [Header( "Camera Zoom" )]
    [SerializeField] private float minZoom = 3f;
    [SerializeField] private float maxZoom = 7f;
    [SerializeField] private float zoomSpeed = 40f;

    private Vector3 lastFramePosition;

    private Camera cam;

    private void Awake() 
    {
        cam = gameObject.GetComponent<Camera>();
    }

    void Update()
    {
        CheckKeyboardScroll();
        CheckCameraZoom();
        CheckMouseScroll();
    }

    private void CheckKeyboardScroll ()
    {
        float translationX = Input.GetAxis( "Horizontal" ) * Time.deltaTime;
        float translationY = Input.GetAxis( "Vertical" ) * Time.deltaTime;

        cam.transform.Translate( translationX * keyboardScrollSpeed, translationY * keyboardScrollSpeed, 0 );
    }

    private void CheckCameraZoom () 
    {
        float mouseZoom = Input.GetAxis( "Mouse ScrollWheel" );

        if ( mouseZoom > 0 && cam.orthographicSize > minZoom )
            cam.orthographicSize -= zoomSpeed * Time.deltaTime;
        else if ( mouseZoom < 0 && cam.orthographicSize < maxZoom )
            cam.orthographicSize += zoomSpeed * Time.deltaTime;

        cam.orthographicSize = Mathf.Clamp( cam.orthographicSize, minZoom, maxZoom );
    }

    private void CheckMouseScroll ()
    {
        Vector3 currentFramePosition = cam.ScreenToWorldPoint( Input.mousePosition );
        currentFramePosition.z = 0;

        if ( Input.GetMouseButton( 2 ) ) 
            cam.transform.Translate( lastFramePosition - currentFramePosition );

        lastFramePosition = cam.ScreenToWorldPoint( Input.mousePosition );
        lastFramePosition.z = 0;
    }
}
