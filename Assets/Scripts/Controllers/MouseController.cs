﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    [Header( "Tile Cursor" )]
    [SerializeField] private Sprite tileCursorSprite;
    private TileCursor tileCursor;

    private Camera mainCam;

    // Mouse positions and dragging behaviour variables
    private Vector3 currentFramePosition;
    private Tile tileUnderMouse;
    private Vector3 dragStartPosition;

    private void Start() 
    {
        mainCam = Camera.main;   

        // Set the Tile Cursor
        tileCursor = new TileCursor( tileCursorSprite, this.transform );

        BuildingPreview.SetCursorParent( gameObject );
    }

    private void Update() 
    {
        currentFramePosition = GetMouseWorldCoordinate();
        tileUnderMouse = WorldController.Instance.GetTileAtWorldPosition( currentFramePosition );

        UpdateTileCursor();

        UpdateDragging();
    }

    private void UpdateTileCursor()
    {
        if ( tileUnderMouse == null )
        {
            tileCursor.Disable();
            return;
        }

        tileCursor.UpdateCursorPosition( new Vector3( tileUnderMouse.x, tileUnderMouse.y, 0 ) );
        tileCursor.Enable();
    }

    private void UpdateDragging () 
    {
        // Drag Start
        if ( Input.GetMouseButtonDown( 0 ) )
            StartDrag();

        // During Drag 
        if ( Input.GetMouseButton( 0 ) )
            ShowDragAreaPreview();

        // Drag End
        if ( Input.GetMouseButtonUp( 0 ) )
            EndDrag();
    }

    private void StartDrag () 
    {
        dragStartPosition = currentFramePosition;
    }

    private void ShowDragAreaPreview () 
    {
        BuildingPreview.CreateArea( new Area2D( dragStartPosition, currentFramePosition ), tileCursorSprite );
    }

    private void EndDrag () 
    {
        new Area2D( dragStartPosition, currentFramePosition ).Loop( ChangeTileTypeToGrass );
        BuildingPreview.ClearArea();
    }

    // TODO: Does not belong here
    private void ChangeTileTypeToGrass( Vector2 position )
    {
        Tile tile = WorldController.Instance.GetTileAtWorldPosition( position );

        if ( tile == null )
            return;

        tile.Type = Tile.TileType.Grass;
    }

    private Vector3 GetMouseWorldCoordinate () 
    {
        Vector3 pos = mainCam.ScreenToWorldPoint(Input.mousePosition);
        pos.z = 0;
        return pos;
    }
}
