﻿using UnityEngine;
using System;

public class Tile
{
    public enum TileType 
    {
        Grass,
        Dirt,
        Stone

    }

    private TileType type;
    public TileType Type 
    {
        get { return type; }
        set 
        {
            if ( type == value )
                return;
            
            type = value;
            onTileTypeChanged?.Invoke( this, this );
        }
    }

    World world;
    public int x { get; private set; } 
    public int y { get; private set; }

    public float height { get; private set; }

    public EventHandler<Tile> onTileTypeChanged;

    public Tile ( World world, int x, int y, float height )
    {
        this.world = world;
        this.x = x;
        this.y = y;
        this.height = height;
    }
}
